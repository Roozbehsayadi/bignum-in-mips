
	.data 
	
num1:	.space 400	# Up to 100 digits
num1_point: .space 400
num2:	.space 400	# Up to 100 digits
num2_point: .space 400
res:	.space 800	# Up to 200 digits

num1_digits:
	.space 4
num2_digits:
	.space 4

num1_temp:
	.space 100	#Used in getting input
num2_temp:
	.space 100	#Used in getting input
	

input_message_num1:
	.asciiz "Enter first number, no more than 100 digits\n"
input_message_num2:
	.asciiz "Enter second number, no more than 100 digits\n"
input_type_message:
	.asciiz "Enter an op code as you wish:\n1: Sum\n2: Multiply\n3: Print first number\n4: Print second number\n5: Exit\n6: check equality\n7: check less than\n"
sum_type_message:
	.asciiz "Enter a number.\n1: Print sum.\n2: Calculate sum and put it in first number.\n3: Calculate sum and put it in second number.\n"
check_equality_message1:
	.asciiz "entered number are equal!\nResult register value is:\n"
check_equality_message2:
	.asciiz "entered number are not equal!\nResult register value is:\n"	
check_less_than_message1:
	.asciiz "input number 1 is less than input number 2\nResult register value is:\n"	
check_less_than_message2:
	.asciiz "input number 1 is not less than input number 2\nResult register value is:\n"	
ENTER_STRING:
	.asciiz "\n"
DEBUG_MESSAGE:
	.asciiz "Here!\n"

num_size:
	.word 100	#numbers are 100 digits at most.


	.globl main 
	
	.text	

main:
	
	jal getTwoNumbersInString	# Gets two numbers in string format
	
	# Now we want to cast the strings to arrays
	# First number:
	la $a0, num1_temp
	la $a1, num1
	la $a2, num1_digits
	lw $a2, ($a2)
	jal castStringToArrayOfInts
	
	# Second number:
	la $a0, num2_temp
	la $a1, num2
	la $a2, num2_digits
	lw $a2, ($a2)
	jal castStringToArrayOfInts
	
	jal startGettingTasks
	
	#Print numbers
	#la $a0, num1
	#jal printNumber
	
	#la $a0, num2
	#jal printNumber
	
	j Exit
	
	
startGettingTasks: 

	addi $sp, $sp, -4	# {
	sw $ra, 0($sp)		# } -> 	Saving return address in the stack (Because we are using jal in the next instructions).

	gettingTasksLoop: 

		li $v0, 4
		la $a0, input_type_message
		syscall	
	
		li $v0, 5
		syscall
		
		move $s0, $v0	# v0 is not preserved by the callee, so we put its value in a preserved register
	
		beq $s0, 1, calculateSumTask
		#beq $s0, 2, calculateMultiply
		beq $s0, 3, printFirstNumberTask
		beq $s0, 4, printSecondNumberTask
		beq $s0, 5, gettingTasksClosure
		beq $s0, 6, checkEqualityTask
		beq $s0, 7, checkLessThanTask
		
		checkLessThanTask:
		
			jal checkLessThan
			j gettingTasksLoop
		
		checkEqualityTask:
		
			jal checkEquality
			j gettingTasksLoop
		
		calculateSumTask:
		
			jal calculateSum
			j gettingTasksLoop
		
		printFirstNumberTask:
			la $a0, num1
			li $a1, 100
			jal printNumber
			j gettingTasksLoop
		
		printSecondNumberTask:
			la $a0, num2
			li $a1, 100
			jal printNumber
			j gettingTasksLoop
			
	gettingTasksClosure:
	
		lw $ra, 0($sp)		# {
		addi $sp, $sp, 4	# } -> Retrieving return address from stack.

		jr $ra
checkLessThan:
	la $s0,num1
	la $s1,num2
	li $s2,0		# check result (by default number 1 is not less than number 2)
	li $s3,100		#counter for loop
	
	checkLessThanLoop:
	beqz $s3,checkLessThanLoopExit
	
	lw $t0, ($s0)		# load one digit from number 1
	lw $t1, ($s1)		# load one digit from number 2
	
	sgt $t3, $t1,$t0			#{
	bne $t3,$zero,lessThan			#} -> if current digit from number 2 is greater then first number is less than second number
	slt $t3, $t1,$t0 				#{	
	bne $t3,$zero , checkLessThanLoopExit		#}-> if current digit of second number is less than current digit of first one then first number is greater
							
	#if non of above branch happens it means current digits of first number and second numbers are equal						
	addi $s0,$s0,4		#point to next digit
	addi $s1,$s1,4		#point to next digit
	
	subi $s3,$s3,1		#counter--
	j checkLessThanLoop
	checkLessThanLoopExit:
	li $v0,4				# if pc branched here or loop ran completly it means first number >= second number
	la $a0, check_less_than_message2	# print msg
	syscall
	
	li $v0,1
	move $a0,$s2
 	syscall		#print result register value 
 
	li $v0, 4		# {
	la $a0, ENTER_STRING	#	Prints a "\n" at the end of the number
	syscall
	
	j checkLessThanExit
	
	lessThan:		# if pc branched here o it means first number < second number
	li $s2,1		# change result value to 1
	
	li $v0,4
	la $a0, check_less_than_message1	#print message
	syscall
	
	li $v0,1
	move $a0,$s2		#print register value
	syscall

	li $v0, 4		# {
	la $a0, ENTER_STRING	#	Prints a "\n" at the end of the number
	syscall

checkLessThanExit:					
		jr $ra
checkEquality:
	
	la $s0,num1
	la $s1,num2
	li $s2,1		# check result
	li $s3,100		# loop counter
	
	checkEqualityLoop:
	beqz $s3,checkEqualityLoopExit
	
	lw $t0, ($s0)		# load one digit from number 1
	lw $t1, ($s1)		# load one digit from number 2
	
	bne $t0,$t1 ,notEqual	#check current digits equality, is they are equal, next digit should be checked
	
	addi $s0,$s0,4		#pointer to next digit of num 1
	addi $s1,$s1,4		#pointer to next digit of num 2
	
	subi $s3,$s3,1		#counter--
	j checkEqualityLoop
	
	checkEqualityLoopExit:		# if loop completly done it means both numbers all equal
	
	
	li $v0,4
	la $a0 , check_equality_message1	#print message
	syscall
	li $v0,1
	move $a0,$s2				#print result value
	syscall
	
	li $v0, 4		# {
	la $a0, ENTER_STRING	#	Prints a "\n" at the end of the number
	syscall
	
	
	j calculateCheckEqualityExit
	
	notEqual:		#if pc branched here it means input numbers are not equal
	li $s2,0
	li $v0,4
	la $a0 , check_equality_message2	#print message
	syscall
	li $v0,1
	move $a0,$s2				#print result register value
	syscall
	
	li $v0, 4		# {
	la $a0, ENTER_STRING	#	Prints a "\n" at the end of the number
	syscall
	
	
	
	calculateCheckEqualityExit:					
		
		jr $ra



calculateSum:

	addi $sp, $sp, -4	# {
	sw $ra, 0($sp)		# } -> 	Saving return address in the stack (Because we are using jal in the next instructions).

	la $s0, num1 		# {
	addi $s0, $s0, 396	# } -> s0: Pointer to end of the first array
	la $s1, num2		# {
	addi $s1, $s1, 396	# } -> s1: Pointer to end of the second array
	la $s2, res		# {
	addi $s2, $s2, 796	# } -> s2: Pointer to end of the result array
	
	li $s3, 0	#s3: carry-in
	li $s4, 100	# s4: for loop counter
	calculateSumLoop:
	
		beqz $s4, calculateSumLoopExit
	
		lw $t0, ($s0)		# t0: Current digit of first number
		lw $t1, ($s1)		# t1: Current digit of second number
		
		add $t2, $t0, $t1	# t2: sum of the digits
		add $t2, $t2, $s3 	# t2: sum of the digits (carry in included)
		
		sgt $s3, $t2, 9		# if sum of the digits was greater that 9, then we have carry out... s3 is our carry out (and carry in for the next digit)
		beq $s3, $zero, calculateSumLoopSaveDigitInResult
		
		subi $t2, $t2, 10
		
		calculateSumLoopSaveDigitInResult:
		
			sw $t2, ($s2)
		
		subi $s0, $s0, 4	# {
		subi $s1, $s1, 4	# 	-> Decreasing array pointers by 4 (bytes)... because of the whole... word array and other stuff... you know? 
		subi $s2, $s2, 4	# }
		
		subi $s4, $s4, 1	# Decreasing loop counter
		
		j calculateSumLoop

	calculateSumLoopExit: 
		
		sw $s3, ($s2)		# Saving the last carry out in the result.
		subi $s2, $s2, 396
		addi $s0, $s0, 4	# {
		addi $s1, $s1, 4	# } -> Fixing the pointers... just in case.

	# Show a message to user, so they can decide what to do with the output
	li $v0, 4
	la $a0, sum_type_message
	syscall
	
	# Get user's prefered operation
	li $v0, 5
	syscall
	
	# Saving the operation user asked for, so we won't lose it in a callee function that may be ahead.
	move $s5, $v0
	
	beq $s5, 1, calculateSumPrint
	beq $s5, 2, calculateSumSetFirstNum
	beq $s5, 3, calculateSumSetSecondNum
	
	calculateSumPrint:
	
		la $a0, ($s2)
		li $a1, 200
		jal printNumber 
		j calculateSumExit
		
	calculateSumSetFirstNum:
	
		# s2: Pointer to first index of result, s0: Pointer to first index of first number
		addi $t0, $s2, 400
		move $a0, $t0
		move $a1, $s0
		li $a2, 100
		jal copyArray
		j calculateSumExit 
		
	calculateSumSetSecondNum:
	
		# s2: Pointer to first index of result, s1: Pointer to first index of first number
		addi $t0, $s2, 400
		move $a0, $t0
		move $a1, $s1
		li $a2, 100
		jal copyArray
		j calculateSumExit

	calculateSumExit:					
		lw $ra, 0($sp)		# {
		addi $sp, $sp, 4	# } -> Retrieving return address from stack.
		
		jr $ra
	

copyArray:

	#a0: Array to be copied from address, a1: Array to be copied to address, a2: number of words to be copied
	move $t0, $a2
	
	copyArrayLoop:
	
		beq $t0, $zero, copyArrayExitLoop
		
		lw $t1, ($a0)
		sw $t1, ($a1)
		
		addi $a0, $a0, 4
		addi $a1, $a1, 4
		subi $t0, $t0, 1
		
		j copyArrayLoop

	copyArrayExitLoop:
	
		jr $ra
	

printNumber:

	#a0: address of a word array, a1: number of words
	la $t0, ($a0)
	move $t1, $a1	# for loop counter
#	li $t1, 100	# for loop counter
	
	printNumbersLoop:
	
		beq $t1, $zero, exitPrintNumber		# if loop counter == 0, exit loop
		subi $t1, $t1, 1			# decrease loop counter by 1
		
		li $v0, 1		# {
		lw $a0, ($t0)		#	Print current character
		syscall			# }
		
		addi $t0, $t0, 4	# Increase array pointer by 4 bytes
		
		j printNumbersLoop
	
	exitPrintNumber:
		
		li $v0, 4		# {
		la $a0, ENTER_STRING	#	Prints a "\n" at the end of the number
		syscall			# }
		
		jr $ra
	
	
castStringToArrayOfInts:
	#a0: string, a1: array, a2: num_of_digits
	
	#t0: number of zeros
	move $t0, $a2
	subi $t0, $t0, 100
	sub $t0, $zero, $t0
	
	addZerosToArrayLoop:
		beqz $t0, exitFromAddZerosToArrayLoop		# Is loop counter zero? Exit from loop!
		sw $zero, ($a1)		# save a zero in the array
		subi $t0, $t0, 1	# decrease number of zeros by 1
		addi $a1, $a1, 4	# increase array pointer by 4 bytes
		
		j addZerosToArrayLoop
		
	exitFromAddZerosToArrayLoop:
		move $t0, $a2		# t0 -> number of digits
	castStringToArrayOfBytesLoop:
		beq $t0, $zero, castingIsCompleted	# Is number of digits == 0? Exit from loop! 
		lb $t2, ($a0)				# Load next character from string
		subi $t2, $t2, 48			# decrease it by 48 (ascii code of '0')
		sb $t2, ($a1)				# Save the retrieved digit in array

		subi $t0, $t0, 1		# Decrease number of digits (we can look at it as loop counter) by 1		
		addi $a0, $a0, 1		# Increase string counter by 1
		addi $a1, $a1, 4		# Increase array counter bt 4 
		
		j castStringToArrayOfBytesLoop
	
	castingIsCompleted:
		jr $ra
	
getTwoNumbersInString:
	
	## Showing a message to user, so user enters the first number. 
	li $v0, 4
	la $a0, input_message_num1
	syscall	
	
	## Getting the first number from user.
	la $a0, num1_temp
	li $a1, 100
	li $v0, 8
	syscall
	
	addi $sp, $sp, -4	# {
	sw $ra, 0($sp)		# } ->	Saving return address in the stack (Because we are using jal in the next instruction).
	
	la $a0, num1_temp
	
	jal calculateNumberDigits

	#lw $ra, 0($sp)		# {
	#addi $sp, $sp, 4	# } -> Retrieving return address from stack.
	
	subi $v0, $v0, 1
	sw $v0, num1_digits
	
	## Showing a message to user, so user enters the second number.
	li $v0, 4
	la $a0, input_message_num2
	syscall
	
	## Getting the second number from user.
	la $a0, num2_temp
	li $a1, 100
	li $v0, 8
	syscall 
	
	#addi $sp, $sp, -4	# {
	#sw $ra, 0($sp)		#	Saving return address in the stack (Because we are using jal in the next instruction).
	#la $a0, num2_temp	# }
	
	jal calculateNumberDigits
	
	lw $ra, 0($sp)		# {
	addi $sp, $sp, 4	# } -> Retrieving return address from stack.
	
	subi $v0, $v0, 1
	sw $v0, num2_digits
	
	## Back to where the program was (return value is written in $ra)
	jr $ra


calculateNumberDigits:

	#a0: string
	la $t0, ($a0)	# Load address of string into t0
	calculateNumberDigitsLoop:
		lb $t1, 0($t0)		# Load the next character of string into t1
		beq $t1, $zero, calculateNumberDigitsLoopEnd	# Is the current character 0? If so, exit from loop
		
		addi $t0, $t0, 1	# Increase string counter by 1
		j calculateNumberDigitsLoop	
		
	calculateNumberDigitsLoopEnd:
		la $t1, ($a0)		# {
		sub $v0, $t0, $t1	# } -> Calculating string length by subtracting two registers
		
		jr $ra

Exit:

	li $v0, 10
	syscall
